type ('a, 'b) either = ('a, 'b) Either.t


module type Profunctor = sig
  type ('a, 'b) t

  val lmap : ('a2 -> 'a1) -> ('a1, 'b) t -> ('a2, 'b) t
  val rmap : ('b1 -> 'b2) -> ('a, 'b1) t -> ('a, 'b2) t
  (* FIXME take this out and define it from lmap and rmap ?
     `let dimap lmap rmap f g x = lmap f @@ rmap g x` *)
  val dimap : ('a2 -> 'a1) -> ('b1 -> 'b2) -> ('a1, 'b1) t -> ('a2, 'b2) t
end


module type Strong = sig
  include Profunctor

  val first : ('a, 'b) t -> ('a * 'c, 'b * 'c) t
  val second : ('a, 'b) t -> ('c * 'a, 'c * 'b) t
end


module type Choice = sig
  include Profunctor

  val left : ('a, 'b) t -> (('a, 'c) either, ('b, 'c) either) t
  val right : ('a, 'b) t -> (('c, 'a) either, ('c, 'b) either) t
end


module type Affine = sig
  type ('a, 'b) t

  include Strong with type ('a, 'b) t := ('a, 'b) t
  include Choice with type ('a, 'b) t := ('a, 'b) t
end


module type Optic_types = sig
  (* from structure s to structure t *)
  type s type t
  (* from substructure a to substructure b *)
  type a type b
end


module type Iso = sig
  include Optic_types

  module Apply : functor (X : Profunctor) -> sig
    val lift : (a, b) X.t -> (s, t) X.t
  end
end


module type Lens = sig
  include Optic_types

  module Apply : functor (X : Strong) -> sig
    val lift : (a, b) X.t -> (s, t) X.t
  end
end


module type Prism = sig
  include Optic_types

  module Apply : functor (X : Choice) -> sig
    val lift : (a, b) X.t -> (s, t) X.t
  end
end


module type Affine_traversal = sig
  include Optic_types

  module Apply : functor (X : Affine) -> sig
    val lift : (a, b) X.t -> (s, t) X.t
  end
end


module Lens = struct
  type ('s, 't, 'a, 'b) t = (module Lens
    with type s = 's
     and type t = 't
     and type a = 'a
     and type b = 'b
  )

  let make (type s' t' a' b') (get : s' -> a') (set : b' -> s' -> t') : (s', t', a', b') t =
    (module struct
      type s = s' type t = t'
      type a = a' type b = b'
     
      module Apply (X : Strong) = struct
        let lift strong =
          X.dimap (fun x -> (get x, x)) (fun (v, x) -> set v x) (X.first strong)
      end
    end)
end


module Prism = struct
  type ('s, 't, 'a, 'b) t = (module Prism
    with type s = 's
     and type t = 't
     and type a = 'a
     and type b = 'b
  )

  let make (type s' t' a' b') (match_ : s' -> (a', t') either) (make : b' -> t') : (s', t', a', b') t =
    (module struct
      type s = s' type t = t'
      type a = a' type b = b'
     
      module Apply (X : Choice) = struct
        let lift choice =
          X.dimap match_ (fun vx -> Either.fold ~left:make ~right:Fun.id vx) (X.left choice)
      end
    end)

  let make' get mk =
    let match_ x =
      match get x with
      | None -> Either.Right x
      | Some a -> Either.Left a
    in
    make match_ mk
end


module Affine_traversal = struct
  type ('s, 't, 'a, 'b) t = (module Affine_traversal
    with type s = 's
     and type t = 't
     and type a = 'a
     and type b = 'b
  )

  let of_lens (type s' t' a b) (l : (s', t', a, b) Lens.t) : (s', t', a, b) t =
    let (module M) = l in
    (module M)

  let of_prism (type s' t' a b) (l : (s', t', a, b) Prism.t) : (s', t', a, b) t =
    let (module M) = l in
    (module M)

  let compose (type s' t' a b a' b') (at1 : (s', t', a', b') t) (at2 : (a', b', a, b) t) :
    (s', t', a, b) t =
    let (module At1) = at1 in
    let (module At2) = at2 in
    (module struct
      type s = s' type t = t'
      type nonrec a = a type nonrec b = b

      module Apply (X : Affine) = struct
        let lift affine =
          let module Ap1 = At1.Apply (X) in
          let module Ap2 = At2.Apply (X) in
          Ap1.lift @@ Ap2.lift affine
      end
    end)
end


type ('a, 'b) ab = { a : 'a ; b : 'b }
type ('c, 'd) cd = C of 'c | D of 'd

let lens_a () = Lens.make (fun x -> x.a) (fun v x -> { x with a=v })
let prism_c () = Prism.make' (function C c -> Some c | D _ -> None) (fun c -> C c)
let at_ac () = Affine_traversal.(compose (of_lens @@ lens_a ()) (of_prism @@ prism_c ()))
